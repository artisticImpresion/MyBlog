<%-- 
    Document   : archive
    Created on : 2017-07-21, 22:13:13
    Author     : artisticImpresion
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>home</title>
        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <%@include file="/WEB-INF/jspf/header.jspf" %>
                </div>
            </div> 
            <div class="row">
                <div class="col-md-12"><%@include file="/WEB-INF/jspf/menu.jspf" %></div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <%@include file="/WEB-INF/jspf/archive.jspf" %>
                </div>
                <div class="col-md-3">
                    <%@include file="/WEB-INF/jspf/right_side.jspf" %>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12"><%@include file="/WEB-INF/jspf/footer.jspf" %></div>
            </div> 
        </div>
        
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
    </body>
</html>